# Copyright 2020-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
inherit go-module systemd
GIT_COMMIT=11c4b121ecfe6a8110712371e4b432fe8b269de5

DESCRIPTION="A nomad task driver plugin for sandboxing workloads in podman containers "
HOMEPAGE="https://github.com/hashicorp/nomad-driver-podman"
SRC_URI="https://github.com/hashicorp/nomad-driver-podman/archive/v${PV}.tar.gz -> ${P}.tar.gz"
SRC_URI+=" https://distfiles.zortaniac.org/${P}-vendor.tar.xz"

LICENSE="MPL-2.0"
SLOT="0"
KEYWORDS="amd64 arm64"

RESTRICT=" test"

BDEPEND="sys-cluster/nomad"

src_compile() {
	local go_ldflags go_tags
	go_ldflags="-X github.com/hashicorp/nomad-driver-podman/version.GitCommit=${GIT_COMMIT}"
	go_tags="codegen_generated"
	CGO_ENABLED=1 \
		ego build \
		-ldflags "${go_ldflags}" \
		-tags "${go_tags}" \
		-trimpath \
		-o bin/${PN}
}

src_install() {
	exeinto /usr/lib/nomad/plugins
	doexe bin/${PN}
	einstalldocs
	dodoc CHANGELOG.md
}
